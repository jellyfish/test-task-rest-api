from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from rest_framework.authtoken.models import Token


class UploadedFile(models.Model):
    """
    Model for uploaded file
    """
    file = models.FileField(default='')
    uploader = models.ForeignKey(User, related_name='files')
    upload_date = models.DateTimeField(auto_now=True)


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """
    Signal for create token for created user
    """
    if created:
        Token.objects.create(user=instance)
