from rest_framework import serializers

from django.contrib.auth.models import User

from .models import UploadedFile


class UploadedFileSerializer(serializers.ModelSerializer):
    """
    Serializer for Uploaded file
    """
    uploader = serializers.PrimaryKeyRelatedField(read_only=True,
                                                  default=serializers.CurrentUserDefault())

    class Meta:
        model = UploadedFile
        fields = [
            'id',
            'file',
            'upload_date',
            'uploader'
        ]


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for users
    """

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
            'url',
            'password'
        ]
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        """
        Override method for create new user with hashed password
        :param validated_data: validated data
        :return: new user
        """
        user = User.objects.create_user(**validated_data)
        return user
