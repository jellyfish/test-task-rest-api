from django.contrib.auth.models import User

from rest_framework import viewsets, permissions

from api import serializers
from api.models import UploadedFile


class UserViewSet(viewsets.ModelViewSet):
    """
    ViewSet for users
    """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class UploadedFileViewSet(viewsets.ModelViewSet):
    """
    ViewSet for Uploaded files with Authenticated CRUD permission
    """
    queryset = UploadedFile.objects.all()
    serializer_class = serializers.UploadedFileSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, ]
