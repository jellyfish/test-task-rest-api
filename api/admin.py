from django.contrib import admin
from .models import UploadedFile


class UploadedFileAdmin(admin.ModelAdmin):
    list_display = ['uploader', 'file', 'upload_date']

admin.site.register(UploadedFile, UploadedFileAdmin)
