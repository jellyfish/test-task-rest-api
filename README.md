### Installing and running
======
1. Clone this repo.
2. Install virtualenv ([instruction](https://virtualenv.pypa.io/en/stable/installation/)).
3. Go into **_test-task-rest-api_** dir.
4. Run **_install.sh_** first.
5. Then run **_start.sh_**.
6. Open [http://127.0.0.1:8000/v1/](http://127.0.0.1:8000/v1/) in your browser.